from django.urls import path
from . import views

app_name = 'JSAccordion'

urlpatterns = [
    path('', views.index, name="index"),
]