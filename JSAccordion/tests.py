from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.


class TestCaseStory7(TestCase):
    def root_url_check(self):
        response = Client().get(reverse('JSAccordion:index'), follow=True)
        self.assertEqual(response.status_code, 200)
    
    def views_check(self):
        response = Client().get(reverse('JSAccordion:index'), follow=True)
        self.assertIn("Accordion", str(response.content))
    
