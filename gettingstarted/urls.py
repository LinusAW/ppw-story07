from django.urls import path, include

from django.contrib import admin

import JSAccordion.views

admin.autodiscover()


urlpatterns = [
    path('', JSAccordion.views.index),
    path('JSAccordion/', include('JSAccordion.urls')),
    path("admin/", admin.site.urls),
]
